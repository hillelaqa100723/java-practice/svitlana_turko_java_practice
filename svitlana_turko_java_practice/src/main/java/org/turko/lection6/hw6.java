package org.turko.lection6;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class hw6 {
    public static void main(String[] args) {

        List<Integer> winNumbers = new ArrayList<>();
        Random random = new Random();

        for (int i = 0; i < 6; i++) {
            while (true) {
                int winningNumber = random.nextInt(36) + 1;
                if (!winNumbers.contains(winningNumber)) {
                    winNumbers.add(winningNumber);
                    break;
                }
            }
        }
        System.out.println(winNumbers);
        System.out.println("Please enter 6 numbers between 1 and 36");
        Scanner scanner = new Scanner(System.in);
        List<Integer> guessedNumbers = new ArrayList<>();

        for (int i = 0; i < 6; i++) {
            System.out.println("Your current numbers are " + guessedNumbers);
            System.out.println("Please enter a number (1-36): ");
            while (true) {
                String numberString = scanner.nextLine();
                int number = Integer.parseInt(numberString);
                if (number >= 1 && number <= 36) {
                    guessedNumbers.add(number);
                    break;
                } else {
                    System.out.println(number + " is not between 1 and 49. Please enter different number");
                }
            }
        }
        System.out.println("The winning numbers were: " + winNumbers);
        System.out.println("Your numbers are: " +guessedNumbers);

        guessedNumbers.retainAll(winNumbers);
        System.out.println("Your matched numbers are: " + guessedNumbers);

        if (guessedNumbers.containsAll(winNumbers)) {
            System.out.println("You are WINNER");
        } else {
            System.out.println("Sorry, you lost. Next time!");
        }
    }
}
