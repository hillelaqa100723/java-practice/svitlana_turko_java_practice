package org.turko.lection3;
import java.util.Scanner;
public class hw1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Type the first value");
        int a = input.nextInt();

        System.out.println("Type the second  value");
        int b = input.nextInt();
        int rez;

        rez = a + b;
        System.out.println("Plus: " + rez);
        rez = a - b;
        System.out.println("Minus: " + rez);
        rez = a * b;
        System.out.println("Mult: " + rez);
        if (b != 0) {
            rez = a / b;
            System.out.println("Div: " + rez);
            rez = a % b;
            System.out.println("Rem: " + rez);
        } else {
            System.out.println("Div and Rem: Cannot divide by zero");
        }

    }
}
