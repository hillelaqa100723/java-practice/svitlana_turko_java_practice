package org.turko.lection7;


import java.util.List;
import java.util.ArrayList;

public class hw7 {
    enum mathAction {
        PLUS("+"),
        MINUS("-"),
        DIVIDE("/"),
        MULTIPLY("*"),
        REMAINDER("%");


        private String value;

        mathAction(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return value;
        }
    }

    public static Double calculate(List<String> data) {

        String mathActionString = data.get(1);
        MathAction mathAction = MathAction.valueOf(mathActionString.toUpperCase());

        Double firstDigit = Double.parseDouble(data.get(0));
        Double secondDigit = Double.parseDouble(data.get(2));
        Double result;

        switch (mathAction) {
            case PLUS:
                result = firstDigit + secondDigit;
                break;
            case MINUS:
                result = firstDigit - secondDigit;
                break;
            case DIVIDE:
                if (secondDigit != 0) {
                    result = firstDigit / secondDigit;
                } else {
                    System.out.println("Cannot divide by zero");
                    return null;
                }
                break;
            case MULTIPLY:
                result = firstDigit * secondDigit;
                break;
            case REMAINDER:
                if (secondDigit != 0) {
                    result = firstDigit % secondDigit;
                } else {
                    System.out.println("Impossible to do it");
                    return null;
                }
                break;
            default:
                return null;
        }
        return result;
    }

    public static String prepareResultString(List<String> data, Double result) {

        String mathActionString = data.get(1);
        MathAction mathAction1 = MathAction.valueOf(mathActionString.toUpperCase());
        Double firstDigit1 = Double.parseDouble(data.get(0));
        Double secondDigit1 = Double.parseDouble(data.get(2));

        switch (mathAction1) {
            case PLUS:
                result = firstDigit1 + secondDigit1;
                break;
            case MINUS:
                result = firstDigit1 - secondDigit1;
                break;
            case DIVIDE:
                if (secondDigit1 != 0) {
                    result = firstDigit1 / secondDigit1;
                } else {
                    System.out.println("Cannot divide by zero");
                    return null;
                }
                break;
            case MULTIPLY:
                result = firstDigit1 * secondDigit1;
                break;
            case REMAINDER:
                if (secondDigit1 != 0) {
                    result = firstDigit1 % secondDigit1;
                } else {
                    System.out.println("Impossible to do it");
                }
                break;
            default:
                return null;
        }
        return String.valueOf(result);

    }

    public static void main(String[] args) {
        List<String> list = new ArrayList<String>();
        list.add("2");
        list.add("MINUS");
        list.add("5");

        System.out.println(prepareResultString(list, calculate(list)));


    }

}
