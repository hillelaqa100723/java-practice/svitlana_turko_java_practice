package org.turko.lection10.some;

public class User {
    private Integer id;
    private String lasName;
    private String firstName;
    private Integer age;

    public User(Integer id, String lasName, String firstName, Integer age, Age age1) {
        this.id = id;
        this.lasName = lasName;
        this.firstName = firstName;
        this.age = age;
    }

    public Integer getId() {
        return id;
    }

    public User setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getLasName() {
        return lasName;
    }

    public User setLasName(String lasName) {
        this.lasName = lasName;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public User setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public Integer getAge() {
        return age;
    }

    public User setAge(Integer age) {
        this.age = age;
        return this;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", lasName='" + lasName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", age=" + age +
                '}';
    }
}
