package org.turko.lection10;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class hw10 {
    private static Integer id;
    private static String firstName;
    private static String lastName;
    private static Integer age;

    public static void main(String[] args) {
        File dataFile = new File("files/data.csv");
        Map<Integer, String> userMap = getDataFromFile(dataFile);


        Map<Integer, String> dataFromFile = getDataFromFile(new File("files/data.csv"));
        System.out.println(getDataById(dataFromFile, 8965));
        System.out.println(getNumberOfOccurrences(dataFromFile, "Ivanov"));
    }

    public static Map<Integer, String> getDataFromFile(File dataFile) {
        try {
            List<String> users = Files.readAllLines(dataFile.toPath());
            Map<Integer, String> usersMap = new HashMap<>();

            for (String user : users) {
                String[] split = user.split(",");
                Integer id = Integer.parseInt(split[0].trim());
                String lastName = split[1].trim();
                String firstName = split[2].trim();
                String age = split[3].trim();

                usersMap.put(id, lastName);
            }
            return usersMap;

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String getDataById(Map<Integer, String> mapData, Integer id) {
        return mapData.get(id);
    }
        public static int getNumberOfOccurrences(Map<Integer, String> mapData, String lastName) {
            int count = 0;

            for (Map.Entry<Integer, String> entry : mapData.entrySet()) {
                String lastNameTemp = entry.getValue().split(" ")[0].trim();
                if (lastNameTemp.equals(lastName)) {
                    count++;
                }
            }
            return count;
        }

        public static List<User> getUsersAgeMoreThen(Map<Integer, User> mapData, int age) {
            List<User> result = new ArrayList<>();
            for (User user : mapData.values()) {
                if (user.getAge() > age) {
                    result.add(user);
                }
            }
            return result;
        }

        public static Map<String, List<Integer>> findEqualsUsers(Map<Integer, User> users) {
            Map<String, List<Integer>> equalUserMap = new HashMap<>();
            Map<String, List<Integer>> tempMap = new HashMap<>();

            for (Map.Entry<Integer, User> entry : users.entrySet()) {
                String nameKey = entry.getValue().getFirstName() + " " + entry.getValue().getLastName();
                if (tempMap.containsKey(nameKey)) {
                    tempMap.get(nameKey).add(entry.getKey());
                } else {
                    List<Integer> idList = new ArrayList<>();
                    idList.add(entry.getKey());
                    tempMap.put(nameKey, idList);
                }
            }

            for (Map.Entry<String, List<Integer>> entry : tempMap.entrySet()) {
                if (entry.getValue().size() > 1) {
                    equalUserMap.put(entry.getKey(), entry.getValue());
                }
            }
            return equalUserMap;
        }
    }

