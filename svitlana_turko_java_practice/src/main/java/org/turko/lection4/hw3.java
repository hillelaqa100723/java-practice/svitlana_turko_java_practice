package org.turko.lection4;

public class hw3 {
    public static void main(String[] args) {
        int[] array = {1, 2, 3, 2, 1};

        boolean palindrome = true;
        for (int i = 0; i < array.length; i++) {
            if (array[i] != array[array.length - i - 1]) {
                palindrome = false;
                break;
            }
        }

        if (palindrome) {
            System.out.println("PALINDROME!");
        } else {
            System.out.println("NOT a palindrome");

        }

    }
}
