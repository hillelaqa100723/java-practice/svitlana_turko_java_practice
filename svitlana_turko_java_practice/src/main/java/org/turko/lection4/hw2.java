package org.turko.lection4;

public class hw2 {
    static boolean isPolindrome(String text) {
        for (int i = 0; i < text.length() / 2; i++) {
            if (text.toLowerCase().charAt(i) != text.toLowerCase().charAt(text.length() - 1 - i)) return false;
        }
        return true;
    }

    public static void main(String args[]) {
        System.out.println(isPolindrome("cat"));
        System.out.println(isPolindrome("pop"));
        System.out.println(isPolindrome("like"));
        System.out.println(isPolindrome("Anna"));
        System.out.println(isPolindrome("Step on no pets"));
    }
}
