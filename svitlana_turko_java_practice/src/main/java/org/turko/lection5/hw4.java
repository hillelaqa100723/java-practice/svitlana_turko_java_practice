package org.turko.lection5;

public class hw4 {
    public static void main(String[] args) {
        int[][] array =
                {
                        {1, 2, 3},
                        {4, 5, 6},
                        {7, 8, 9}
                };
        int size = 3;
        System.out.println("Main diagonal: ");
        for (int i = 0; i < size; i++) {
            System.out.print(array[i][i] + " ");
        }

        System.out.println("");
        System.out.println("Side diagonal: ");
        for (int i = 0; i < size; i++) {
            System.out.print(array[i][size - i - 1] + " ");
        }
    }
}

