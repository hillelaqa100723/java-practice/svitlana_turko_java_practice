package org.turko.lection5;

public class hw5n {
    public static void main(String[] args) {
        int[][] array = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };
        sum(array);
    }
    public static int[] sum(int[][] array) {
        int[] sum = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                sum[i] += array[i][j];
            }
        }
        for (int s:sum) {
            System.out.println(s);
        }
        return sum;
    }
}
